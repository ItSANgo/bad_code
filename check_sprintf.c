/*
 * Bad code.
 */

#include <sysexits.h>
#include <err.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

int
usage(int exit_status, FILE *fp, const char *command_name)
{
     fprintf(fp, "Usage: %s string\n", command_name);
     exit(exit_status);		/*NOTREACHED*/
}


int
main(int argc, char *argv[])
{
     if (argc < 2) {
	  usage(EX_USAGE, stderr, argv[0]);
     }
     size_t argument_length = strlen(argv[1]);
     size_t new_buffer_size = argument_length * 2 + 1;
     unsigned char *buffer = calloc(new_buffer_size, 1);
     if (buffer == NULL) {
	  err(EX_OSERR, "calloc(%llu, 1)",
	      (unsigned long long)new_buffer_size); /*NOTREACHED*/
     }
     strcpy(buffer, argv[1]);
     for (size_t number = 0; number < argument_length; number += 2) {
	  if (snprintf(buffer, new_buffer_size, "%02x%s",
		       buffer[number], buffer + number + 1u) < 0) {
	       err(EX_SOFTWARE, "snpfintf");
	  }
     }
     if (fputs(buffer, stdout) < 0) {
	  err(EX_IOERR, "fputs(%s)", buffer);
     }
     return EX_OK;
}
