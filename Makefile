#

#

PROGRAMS = bad_code
SRCS = check_sprintf.c

TARGETS = $(PROGRAMS)
OBJS = $(SRCS:.c=.o)

all: $(TARGETS)

bad_code: $(OBJS)
	$(CC) -o $@ $(OBJS)

clean:
	rm -f $(TARGETS) $(OBJS) a.out *~
